import Vue from 'vue'
import App from './App.vue'

new Vue({
    el: "#app",
    name: 'app',
    template: '<App/>',
    render: h => h(App),
})

